package simple

import io.javalin.Javalin
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SchemaUtils.create
import org.jetbrains.exposed.sql.transactions.transaction
import java.sql.Connection

object Members : Table("MEMBERS") {
        val id    = integer("id").autoIncrement().primaryKey()
        val money = integer("money")
}

data class Member(val id : Int, val money : Int)

fun main(args: Array<String>) {
    initDatabase()
    val app = Javalin.create().port(7000).start()

    with(app) {
        get("/") { ctx ->
            ctx.json("Hello, World!")
        }
        get("/addMember") { ctx ->
            val money = ctx.request().getParameter("money")?.toIntOrNull()
            if (money != null)
            {
                val id = addMember(money)
                if (id != null) {
                    ctx.json(Member(id, money))
                } else {
                    ctx.status(404)
                }
            } else {
                ctx.result("get parameters error")
            }
        }
        get("/members/:id") { ctx ->
            val id = ctx.param("id")?.toIntOrNull()
            if (id != null)
            {
                val member = getMember(id)
                if (member != null)
                {
                    ctx.json(member)
                } else {
                    ctx.status(404)
                }
            } else {
                ctx.status(400)
            }
        }
        get("/members") { ctx ->
            ctx.json(getAllMembers())
        }
        get("/transfer") { ctx ->
            val id1 = ctx.request().getParameter("id1")?.toIntOrNull()
            val id2 = ctx.request().getParameter("id2")?.toIntOrNull()
            val money = ctx.request().getParameter("money")?.toIntOrNull()

            if (id1 != null && id2 != null && money != null) {
                val transfer = transfer(id1, id2, money)
                ctx.json(transfer)
            } else {
                ctx.result("get parameters error")
            }
        }
    }
}

fun initDatabase() {
    Database.connect("jdbc:h2:mem:default;DB_CLOSE_DELAY=-1",
            driver = "org.h2.Driver",
            setupConnection = {conn ->
                conn.transactionIsolation = Connection.TRANSACTION_READ_COMMITTED
                conn.autoCommit = true
            })
    transaction {
        create (Members)
    }
}

fun transfer(id1: Int, id2: Int, moneyTrans: Int) : Boolean {
    var result = false

    var id11 = id1
    var id22 = id2
    var moneyTrans2  = moneyTrans
    if (id1 > id2) {
        id11 = id2
        id22 = id1
        moneyTrans2 = -moneyTrans2
    }

    transaction {
        val members = Members
                .select({ Members.id.eq(id11) or Members.id.eq(id22) })
                .forUpdate()
                .map { Member(it[Members.id], it[Members.money]) }
        val m1 = members.firstOrNull { it.id == id11 }
        val m2 = members.firstOrNull { it.id == id22 }
        if (m1 != null && m2 != null) {
            if (m1.money - moneyTrans2 > 0 && m2.money + moneyTrans2 > 0) {
                Members.update({Members.id eq id11}) {
                    it[money] = m1.money - moneyTrans2
                }
                Members.update({Members.id eq id22}) {
                    it[money] = m2.money + moneyTrans2
                }
                result = true
            }
        }
    }
    return result
}

fun addMember(mm : Int) : Int? {
    var id : Int? = null
    transaction {
        id = Members.insert {
            it[money] = mm
        } get Members.id

    }
    return id
}

fun getMember(id: Int) : Member? {
    return transaction {
        return@transaction Members.select({Members.id.eq(id)}).map { Member(it[Members.id], it[Members.money]) }.firstOrNull()
    }
}

fun getAllMembers() : List<Member>{
    return transaction {
        return@transaction Members.selectAll().map { Member(it[Members.id], it[Members.money]) }
    }
}
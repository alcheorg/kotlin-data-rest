

import io.restassured.RestAssured.given
import io.restassured.builder.RequestSpecBuilder
import io.restassured.http.ContentType
import io.restassured.specification.RequestSpecification
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.launch
import org.hamcrest.CoreMatchers
import org.junit.BeforeClass
import org.junit.Test
import simple.main

class ApplicationTest {

    companion object
    {
        var spec: RequestSpecification? = null

        @JvmStatic
        @BeforeClass
        fun initSpec()
        {

            spec = RequestSpecBuilder()
                    .setContentType(ContentType.JSON)
                    .setBaseUri("http://localhost:7000/")
                    .build()
            main(emptyArray())
        }
    }


    @Test
    fun testHello() {
        given()
                .spec(spec)
                .get("/")
                .then().body(CoreMatchers.containsString("Hello, World!"))
    }

    @Test
    fun testMoneySafety() {
        val id1 = addMember()
        val id2 = addMember()
        val id3 = addMember()

        List(1000) {
            transferOneAsync(id1, id2)
            transferOneAsync(id2, id3)
            transferOneAsync(id3, id1)
        }

        Thread.currentThread().join(5000)

        val moneySum  = given()
                .spec(spec)
                .get("/members")
                .jsonPath().getList("money", Int::class.java).sum()
        assert(moneySum == 3000)
    }


    private fun transferOneAsync(id1: Int, id2: Int)
    {
        launch(CommonPool) {
            given()
                    .spec(spec)
                    .params(mapOf("id1" to id1, "id2" to id2, "money" to 1)).get("/transfer")
                    .then()
                    .assertThat().body(CoreMatchers.containsString("true"))
        }
    }

    private fun addMember(money: Int = 1000): Int
    {
        return given()
                .spec(spec)
                .param("money", money).get("/addMember")
                .jsonPath().getInt("id")
    }
}